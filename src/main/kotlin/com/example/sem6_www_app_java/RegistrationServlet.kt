package com.example.sem6_www_app_java

import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

@WebServlet(name = "RegistrationServlet", value = ["/register"])
class RegistrationServlet : HttpServlet() {

    override fun doGet(request: HttpServletRequest, response: HttpServletResponse) {
    }

    override fun doPost(request: HttpServletRequest, response: HttpServletResponse) {
        val name = request.getParameter("name")
        val surname = request.getParameter("surname")
        val age = request.getParameter("age")
        val interests = request.getParameterValues("interests")
        var interestsListed = ""
        for (interest in interests) {
            interestsListed += "$interest "
        }

        response.contentType = "text/html"
        response.writer.write("""
            <html>
                <body>
                    <h1>Registration Details</h1>
                    <p><strong>Name:</strong> $name</p>
                    <p><strong>Surname:</strong> $surname</p>
                    <p><strong>Age:</strong> $age</p>
                    <p><strong>Interests:</strong> ${interestsListed}</p>
                </body>
            </html>
        """.trimIndent())
    }
}
