package com.example.sem6_www_app_java

import jakarta.servlet.annotation.WebServlet
import jakarta.servlet.http.HttpServlet
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import java.lang.Math.random


@WebServlet(name = "addNumber", value = ["/add-number"])
class AddNumber : HttpServlet() {
    private lateinit var message: String

    override fun init() {
        message = "Hello World!"
    }

    public override fun doGet(request: HttpServletRequest, response: HttpServletResponse) {
        val numbers = runCatching {
            servletContext.getAttribute(LIST_ATTRIBUTE) as? MutableList<Int>
                ?: throw IllegalArgumentException("Wrong list type")
        }.onSuccess {
            it.add((random() * 100).toInt())
            servletContext.setAttribute(LIST_ATTRIBUTE, it)
        }.onFailure {
            it.printStackTrace()
        }.fold({ it }, { "list empty" })

        response.contentType = "text/html"
        val out = response.writer
        out.println("<html><body>")
        out.println("<h1>$message</h1>")
        out.println("<h1>$numbers</h1>")
        out.println("</body></html>")
    }

    override fun destroy() {
    }
}