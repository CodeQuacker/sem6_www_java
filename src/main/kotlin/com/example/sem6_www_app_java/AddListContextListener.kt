package com.example.sem6_www_app_java

import jakarta.servlet.ServletContextEvent
import jakarta.servlet.ServletContextListener
import jakarta.servlet.annotation.WebListener

const val LIST_ATTRIBUTE = "numbersList"
@WebListener
class AddListContextListener : ServletContextListener {
    private var numbers: MutableList<Int> = mutableListOf()
    override fun contextInitialized(sce: ServletContextEvent) {
        sce.servletContext.setAttribute(LIST_ATTRIBUTE, numbers)
        sce.servletContext.addListener(SortOnChangeAttributeListener())
    }

    override fun contextDestroyed(sce: ServletContextEvent) {
        sce.servletContext.removeAttribute(LIST_ATTRIBUTE)
    }
}