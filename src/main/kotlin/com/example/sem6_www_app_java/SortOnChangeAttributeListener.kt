package com.example.sem6_www_app_java

import jakarta.servlet.ServletRequestEvent
import jakarta.servlet.ServletRequestListener
import jakarta.servlet.annotation.WebListener

@WebListener
class SortOnChangeAttributeListener : ServletRequestListener {
    override fun requestInitialized(sre: ServletRequestEvent) {
        runCatching {
            sre.servletContext.getAttribute(LIST_ATTRIBUTE) as MutableList<Int>
        }.onSuccess {
            it.sort()
            sre.servletContext.setAttribute(LIST_ATTRIBUTE, it)
        }.onFailure {
            it.printStackTrace()
        }
    }
}