<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>

    <style>
        .form-container {
            display: flex;
            flex-direction: column;
            align-items: center;
        }

        .registration-form {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            max-width: 500px;
            margin: 0 auto;
        }

        .form-group {
            display: flex;
            flex-direction: column;
            margin-bottom: 1rem;
        }

        .form-group label {
            margin-bottom: 0.5rem;
        }

        .combo-label {
            margin-top: 0.5rem;
        }
    </style>
</head>
<body>
    <div class="form-container">
        <h1>User Registration Form</h1>
        <form action="register" method="post" class="registration-form">
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" id="name" name="name">
            </div>
            <div class="form-group">
                <label for="surname">Surname:</label>
                <input type="text" id="surname" name="surname">
            </div>
            <div class="form-group">
                <label for="age">Age:</label>
                <input type="number" id="age" name="age">
            </div>
            <div class="form-group">
                <label for="interests" class="combo-label">Interests:</label>
                <select id="interests" name="interests" multiple>
                    <option value="programming">Programming</option>
                    <option value="movies">Movies</option>
                    <option value="books">Books</option>
                    <option value="photography">Photography</option>
                </select>
            </div>
            <div class="form-group">
                <button type="submit">Submit</button>
            </div>
        </form>
    </div>
</body>
</html>